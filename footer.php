<?php
/**
 * footer.php
 *
 * The template for displaying the footer.
 */
?>
               <!-- Modal footer start -->
                <div class="modal-footer">

                    <div class="inquiry-info">
                        <div class="inquiry-info-sign hidden-xs">!</div>
                        <p>Please note that this is not an actual reservation, but only a request for one. <br/>
                            <strong>We will contact you with a confirmation shortly. Thank you!</strong></p>
                    </div>
                    <button type="submit" class="btn btn-inquiry-submit">Check Availability</button>

                </div>
                <!-- Modal footer end -->

            </form>
        </div>
    </div>
</div>
<!--Inquiry Modal end-->


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>

<script src="bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
<script src="bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="bower_components/jquery-animatenumber/jquery.animateNumber.min.js"></script>
<script src="bower_components/bootstrap-3-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Google Maps Api -->
<script style="" type="text/javascript" src="http://maps.google.com/maps/api/js"></script>


<script src="js/custom.js"></script>

</body>
</html>