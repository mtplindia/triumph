<?php

function triumphlibrary_assets() {
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/bower_components/bootstrap/dist/css/bootstrap.min.css', array(), '1.0.0', 'all');
	wp_enqueue_style( 'bootstrap-datepicker', get_template_directory_uri() . 'bower_components/bootstrap-3-datepicker/dist/css/bootstrap-datepicker.min.css', array(), '1.0.0', 'all');
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . 'bower_components/font-awesome/css/font-awesome.min.css', array(), '1.0.0', 'all');
	wp_enqueue_style( 'animate', get_template_directory_uri() . 'bower_components/animate.css/animate.min.css', array(), '1.0.0', 'all');
	wp_enqueue_style( 'simple-line-icons', get_template_directory_uri() . 'bower_components/simple-line-icons/css/simple-line-icons.css', array(), '1.0.0', 'all');
	wp_enqueue_style( 'styles', get_template_directory_uri() . 'css/styles.css', array(), '1.0.0', 'all');

	wp_enqueue_scripts( 'styles', get_template_directory_uri() . 'bower_components/jquery/dist/jquery.min.js', array(), '1.0.0', true);



	
}

add_action( 'wp_enqueue_scripts', 'triumphlibrary_assets' );





?>