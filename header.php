<?php
/**
 * header.php
 *
 * The header for the theme.
 */
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Triumph Library</title>
    <meta name="description"
          content="Accomodation is developed for hotels, motels, guest house and accommodation planning like Vacation Rentals, Homes, Apartments & Rooms and much more.">
    <meta name="author" content="Themeinjection.com">

    <?php wp_head(); ?>

    

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="img/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="img/ico/favicon.png">

</head>
<body>
<div id="page-top"></div>

<!--Navigation Top start-->
<section class="top-navbar container navbar-fixed-top">
    <nav class="navbar navbar-default" id="navigation-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!--Brand / Logo start-->
                <a class="navbar-brand scroll-to" href="#page-top">
                    <img src="img/navbar-logo.png" class="img-responsive" alt="Accommodation Landing Page"/>
                </a>
                <!--Brand / Logo end-->
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <!-- Nav-Links start -->
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a class="scroll-to" href="#sc-rooms">Rooms & Facilities</a></li>
                    <li><a class="scroll-to" href="#sc-attractions">Things to Do</a></li>
                    <li><a class="scroll-to" href="#sc-reviews">Reviews</a></li>
                    <li><a class="scroll-to" href="#sc-rates">Rates & Bookings</a></li>
                    <li><a class="scroll-to" href="#sc-contact">Contact</a></li>
                    <li>
                        <!--Language-Select start-->
                        <div class="language-select">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <img src="img/flags/United-Kingdom.png" alt="English"/> English <i class="fa fa-sort"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-language-select" role="menu">
                                <li><a href="#"><img src="img/flags/Germany.png" alt="German"/> German</a></li>
                                <li><a href="#"><img src="img/flags/France.png" alt="Franch"/> Franch</a></li>
                            </ul>
                        </div>
                        <!--Language-Select end-->
                    </li>
                </ul>
                <!-- Nav-Links end -->
            </div>
        </div>
    </nav>
</section>
<!--Navigation Top end-->